# 20230822-FaizAnsari-NYCSchools



## NYC App

NYC School application which shows list of schools in NYC area. The app is build in Native Android using MVVM architecture.

## Tech stack & Open-source libraries

- Minimum SDK level 24
- [Kotlin](https://kotlinlang.org/) based
- [Coroutines](https://github.com/Kotlin/kotlinx.coroutines)
- [Flow](https://kotlinlang.org/api/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.flow/) for asynchronous.
- [Koin](https://insert-koin.io/) for dependency injection.
- Jetpack
  - [Compose](https://developer.android.com/jetpack/compose?gclid=CjwKCAjw64eJBhAGEiwABr9o2BRlFiZCYMbzXNFymS-fc9pxa66UPYGxS_MqRZsfAQnok2Dxyw-RgRoCHCsQAvD_BwE&gclsrc=aw.ds), A modern toolkit for building native Android UI
  - ViewModel - UI related data holder, lifecycle aware.

- Architecture
   - MVVM Architecture (Model - View - ViewModel)
- [Retrofit2](https://github.com/square/retrofit) - construct the REST APIs.
- [Mockito](https://site.mockito.org/) - mocking framework for unit testing.

## App Screenshot

<table>
  <td>
    <p align="center">
<h3>School List Screen</h1>
      <img src="/screenshots/school_list.png" alt="School List Screen" width="400" height="800"/>
    </p>
  </td>
  <td>
    <p align="center">
<h3>School Detail Screen</h1>
      <img src="/screenshots/school_details.png" alt="School Detail Screen" width="400" height="800"/>
    </p>
  </td>
</table>


