package com.app.nycschooldemo.repositories

import com.app.nycschooldemo.testdata.SchoolTestData.satScoreTestData
import com.app.nycschooldemo.testdata.SchoolTestData.schoolTestData
import com.app.nycschooldemo.models.SATScoreInfoDTO
import com.app.nycschooldemo.network.ApiService
import javax.net.ssl.HttpsURLConnection
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.single
import kotlinx.coroutines.runBlocking
import okhttp3.MediaType
import okhttp3.ResponseBody
import org.junit.Assert.assertEquals
import org.junit.Test
import org.mockito.ArgumentMatchers.anyString
import org.mockito.Mockito.`when`
import org.mockito.kotlin.mock
import retrofit2.Response

@ExperimentalCoroutinesApi
class SchoolRepositoryImplTest {

    private val apiService = mock<ApiService>()
    private val schoolRepository = SchoolRepositoryImpl(apiService)

    // Your byte array content
    private var byteArray = "Api Failed Error".toByteArray()

    // Create a MediaType (specify the type of content, such as text/plain)
    private var mediaType = MediaType.parse("text/plain")

    // Create a ResponseBody using the byte array and MediaType
    private var responseBody: ResponseBody = ResponseBody.create(mediaType, byteArray)

    @Test
    fun `when getSchoolsData() success then return school data`() = runBlocking {
        val mockResponse = Response.success(schoolTestData)
        `when`(apiService.getSchoolsData()).thenReturn(mockResponse)
        val result = schoolRepository.getSchoolsList()
        assertEquals(mockResponse.body(), result.single().data)
    }

    @Test
    fun `when getSchoolsData() failed then return status failed`() = runBlocking {
        `when`(apiService.getSchoolsData()).thenReturn(
            Response.error(HttpsURLConnection.HTTP_NOT_FOUND,responseBody)
        )
        val result = schoolRepository.getSchoolsList()
        assertEquals(HttpsURLConnection.HTTP_NOT_FOUND,result.single().responseCode)
    }

    @Test
    fun `when getSATData() success then return sat score data`() = runBlocking {
        val satScoreList = satScoreTestData
        val mockResponse: Response<List<SATScoreInfoDTO>> = Response.success(satScoreList)
        `when`(apiService.getSATData(anyString())).thenReturn(mockResponse)
        val result = schoolRepository.getSATData("1234")
        assertEquals(mockResponse.body(), result.single().data)
    }

    @Test
    fun `when getSATData() failed then return status failed`() = runBlocking {
        `when`(apiService.getSATData(anyString())).thenReturn(
            Response.error(HttpsURLConnection.HTTP_NOT_FOUND,responseBody)
        )
        val result = schoolRepository.getSATData("1234")
        assertEquals(HttpsURLConnection.HTTP_NOT_FOUND,result.single().responseCode)
    }
}
