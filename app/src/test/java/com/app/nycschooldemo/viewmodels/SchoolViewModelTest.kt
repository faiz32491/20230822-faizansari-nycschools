package com.app.nycschooldemo.viewmodels

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.app.nycschooldemo.MainCoroutineRule
import com.app.nycschooldemo.network.NetworkResult
import com.app.nycschooldemo.repositories.SchoolRepository
import com.app.nycschooldemo.testdata.SchoolTestData.satScoreTestData
import com.app.nycschooldemo.testdata.SchoolTestData.schoolTestData
import com.app.nycschooldemo.ui.views.UiState
import com.app.nycschooldemo.viewmodel.SchoolViewModel
import javax.net.ssl.HttpsURLConnection
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import org.mockito.Mockito.`when`
import org.mockito.kotlin.whenever

@ExperimentalCoroutinesApi
class SchoolViewModelTest {
    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    val mainCoroutineRule = MainCoroutineRule()

    private val repository = mock(SchoolRepository::class.java)
    private lateinit var viewModel: SchoolViewModel

    @Before
    fun setup() {
        runBlocking {
            whenever(repository.getSchoolsList()).thenReturn(flowOf(NetworkResult.Success(schoolTestData)))
        }
        viewModel = SchoolViewModel(repository)
        Dispatchers.setMain(UnconfinedTestDispatcher())
    }

    @Test
    fun `given testdata when fetchSchoolList() called then return school list data`() = runBlocking {
        val mockSchoolList = schoolTestData
        `when`(repository.getSchoolsList()).thenReturn(flowOf(NetworkResult.Success(mockSchoolList)))
        viewModel.fetchSchoolList()
        assertEquals(mockSchoolList, viewModel.schoolList.value)
    }

    @Test
    fun `given fetchSchoolList() when api failed then return error state `() = runBlocking {
        `when`(repository.getSchoolsList()).thenReturn(
            flowOf(
                NetworkResult.Failure(
                    message = "Api Error",
                    responseCode =  HttpsURLConnection.HTTP_NOT_FOUND,
                ),
            )
        )
        viewModel.fetchSchoolList()
        assertEquals(UiState.Error, viewModel.uiState.value)
    }

    @Test
    fun `when fetchSchoolList() called and response is empty data then return empty state`() = runBlocking {
        `when`(repository.getSchoolsList()).thenReturn(flowOf(NetworkResult.Success(listOf())))
        viewModel.fetchSchoolList()
        assertEquals(UiState.Empty, viewModel.uiState.value)
    }

    @Test
    fun `given dbn when fetchSATData() called then return sat data`() = runBlocking {
        val mockSATScoreInfo = satScoreTestData
        `when`(repository.getSATData("1234")).thenReturn(
            flowOf(
                NetworkResult.Success(
                    mockSATScoreInfo
                )
            )
        )
        viewModel.fetchSATData("1234")
        assertEquals(mockSATScoreInfo.firstOrNull(), viewModel.satScoreData.value)
    }

    @Test
    fun `given fetchSATData() when api failed then return error state `() = runBlocking {
        `when`(repository.getSATData("1234")).thenReturn(
            flowOf(
                NetworkResult.Failure(
                    "Api Error",
                    HttpsURLConnection.HTTP_NOT_FOUND,
                ),
            ),
        )
        viewModel.fetchSATData("1234")
        assertEquals(UiState.Error, viewModel.uiState.value)
    }

    @Test
    fun `when fetchSATData() called and response is empty data then return empty state`() = runBlocking {
        `when`(repository.getSATData("1234")).thenReturn(
            flowOf(
                NetworkResult.Success(
                    listOf()
                )
            )
        )
        viewModel.fetchSATData("1234")
        assertEquals(UiState.Empty, viewModel.uiState.value)
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }
}
