package com.app.nycschooldemo.testdata

import com.app.nycschooldemo.models.SATScoreInfoDTO
import com.app.nycschooldemo.models.SchoolInfoDTO

object SchoolTestData {
    val schoolTestData = listOf(
        SchoolInfoDTO(
            city = "Brooklyn",
            dbn = "1234",
            latitude = "0.123456",
            location = "1234 Brooklyn",
            longitude = "0.123456",
            overview_paragraph = "Mission Of School",
            phone_number = "123-456-7890",
            primary_address_line_1 = "1234 Brooklyn",
            school_email = "abc@gmail.com",
            school_name = "ABC School",
            state_code = "123456",
            zip = "1111111",
        ),
    )

    val satScoreTestData = listOf(
        SATScoreInfoDTO(
            dbn = "1234",
            school_name = "NYC School",
            num_of_sat_test_takers = "100",
            sat_math_avg_score = "110",
            sat_critical_reading_avg_score = "120",
            sat_writing_avg_score = "130",
        ),
    )
}
