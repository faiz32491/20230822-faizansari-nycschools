package com.app.nycschooldemo.utils

/**
 * Network call related constants.
 */
object NetworkConstants {
    const val BaseURL = "https://data.cityofnewyork.us/"
}
