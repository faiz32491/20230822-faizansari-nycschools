package com.app.nycschooldemo.utils

import android.content.Context
import android.content.Intent
import android.net.Uri

object Utilities {

    /**
     * Function to launch dial intent
     * @param context - Context used for launching the intent
     * @param phoneNumber - Phone number to dial
     */
    fun dialPhoneNumber(phoneNumber: String?, context: Context) {
        val intent = Intent(Intent.ACTION_DIAL,Uri.parse("tel:$phoneNumber"))
        context.startActivity(intent)
    }

    /**
     * Function to launch Google map from current location to the provided lat/long.
     * @param context - Context used for launching the intent
     * @param latitude - Destination's latitude
     * @param longitude - Destination's longitude
     */
    fun navigateToMaps(context: Context, latitude: String?, longitude: String?) {
        val gmmIntentUri =
            Uri.parse("google.navigation:q=$latitude,$longitude")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        context.startActivity(mapIntent)
    }

    /**
     * function return the address in required format
     */
    fun getAddress(primaryaddress: String, city: String, state: String, zip: String): String {
        return "$primaryaddress\n$city\n$state - $zip"
    }
}