package com.app.nycschooldemo.navigation

sealed class Route(
    val route: String,
) {
    object Home : Route("home")

    object SchoolDetails :
        Route(route = "details/{schoolInfo}") {
        const val parameter = "schoolInfo"
        fun getRoute(schoolInfo: String) = "details/$schoolInfo"
    }
}
