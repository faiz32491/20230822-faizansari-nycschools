package com.app.nycschooldemo.navigation

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.padding
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.app.nycschooldemo.models.SchoolInfoDTO
import com.app.nycschooldemo.ui.views.schooldetails.SchoolDetailsScreen
import com.app.nycschooldemo.ui.views.schoollist.SchoolListScreen
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import java.net.URLDecoder
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

@Composable
fun NavigationGraph(
    navController: NavHostController,
    innerPadding: PaddingValues,
) {
    NavHost(
        navController = navController,
        startDestination = Route.Home.route,
        Modifier.padding(innerPadding),
    ) {
        composable(
            Route.Home.route,
        ) {
            SchoolListScreen { schoolInfo ->
                // convert and encode the data class to string to pass in to school details
                val moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory()).build()
                val jsonAdapter = moshi.adapter(SchoolInfoDTO::class.java).lenient()
                val schoolInfoObject = jsonAdapter.toJson(schoolInfo)
                val encodedData = URLEncoder.encode(schoolInfoObject, StandardCharsets.UTF_8.toString())
                navController.navigate(Route.SchoolDetails.getRoute(encodedData))
            }
        }

        composable(
            Route.SchoolDetails.route,
            arguments = listOf(
                navArgument(Route.SchoolDetails.parameter) {
                    type = NavType.StringType
                },
            ),
        ) { backStackEntry ->
            backStackEntry.arguments?.getString(Route.SchoolDetails.parameter)?.let {
                // decode the data clas from string to json
                val moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory()).build()
                val jsonAdapter = moshi.adapter(SchoolInfoDTO::class.java).lenient()
                val decodedData = URLDecoder.decode(it, StandardCharsets.UTF_8.toString())
                val schoolInfoObject = jsonAdapter.fromJson(decodedData)
                if (schoolInfoObject != null) {
                    SchoolDetailsScreen(schoolInfoObject)
                }
            }
        }
    }
}
