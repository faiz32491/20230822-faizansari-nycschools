package com.app.nycschooldemo.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.app.nycschooldemo.models.SATScoreInfoDTO
import com.app.nycschooldemo.models.SchoolInfoDTO
import com.app.nycschooldemo.network.NetworkResult
import com.app.nycschooldemo.repositories.SchoolRepository
import com.app.nycschooldemo.ui.views.UiState
import kotlinx.coroutines.launch
import org.koin.core.component.KoinComponent

class SchoolViewModel(private val repository: SchoolRepository) : ViewModel(), KoinComponent {

    private val _uiState: MutableLiveData<UiState> = MutableLiveData()
    val uiState: LiveData<UiState> = _uiState

    private val _schoolList: MutableLiveData<List<SchoolInfoDTO>?> = MutableLiveData(listOf())
    val schoolList: LiveData<List<SchoolInfoDTO>?> = _schoolList

    private val _satScoreData: MutableLiveData<SATScoreInfoDTO> = MutableLiveData()
    val satScoreData: LiveData<SATScoreInfoDTO> = _satScoreData

    init {
        fetchSchoolList()
    }

    /**
     * fetch the list of schools.
     */
    fun fetchSchoolList() {
        viewModelScope.launch {
            setUiState(UiState.Loading)
            repository.getSchoolsList().collect { response ->
                when (response) {
                    is NetworkResult.Success -> {
                        if (response.data.isNullOrEmpty()) {
                            setUiState(UiState.Empty)
                        } else {
                            _schoolList.value = response.data
                            setUiState(UiState.Success)
                        }
                    }

                    is NetworkResult.Failure -> {
                        setUiState(UiState.Error)
                    }
                }
            }
        }
    }

    /**
     * function to set UIState when api called
     */
    private fun setUiState(state: UiState) {
        _uiState.postValue(state)
    }

    /**
     * fetch SAT data based on "dpn"
     */
    fun fetchSATData(dbn: String) {
        viewModelScope.launch {
            setUiState(UiState.Loading)
            repository.getSATData(dbn).collect { response ->
                when (response) {
                    is NetworkResult.Success -> {
                        if (response.data.isNullOrEmpty()) {
                            setUiState(UiState.Empty)
                        } else {
                            _satScoreData.value = response.data.firstOrNull()
                            setUiState(UiState.Success)
                        }
                    }

                    is NetworkResult.Failure -> {
                        setUiState(UiState.Error)
                    }
                }
            }
        }
    }
}
