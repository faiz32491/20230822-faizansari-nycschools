package com.app.nycschooldemo.models

import com.squareup.moshi.JsonClass

/**
 * Data class for school list api response.
 */
@JsonClass(generateAdapter = true)
data class SchoolInfoDTO(
    val city: String?,
    val dbn: String?,
    val latitude: String?,
    val location: String?,
    val longitude: String?,
    val phone_number: String?,
    val primary_address_line_1: String?,
    val school_email: String?,
    val school_name: String?,
    val state_code: String?,
    val zip: String?,
    val overview_paragraph: String?
)
