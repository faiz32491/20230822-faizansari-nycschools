package com.app.nycschooldemo.di

import com.app.nycschooldemo.network.ApiService
import com.app.nycschooldemo.repositories.SchoolRepository
import com.app.nycschooldemo.repositories.SchoolRepositoryImpl
import com.app.nycschooldemo.utils.NetworkConstants
import com.app.nycschooldemo.viewmodel.SchoolViewModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Koin module to provide Networking, Repositories and ViewModel objects.
 */
// JSON converter
val moshi: Moshi = Moshi.Builder().addLast(KotlinJsonAdapterFactory()).build()
val converterFactory: MoshiConverterFactory = MoshiConverterFactory.create(moshi)

val appModule = module {
    // Provide an instance of Retrofit client with singleton scope
    single {
        Retrofit.Builder()
            .baseUrl(NetworkConstants.BaseURL)
            .addConverterFactory(converterFactory)
            .build()
            .create(ApiService::class.java)
    }
    // Provide an instance of Repository in singleton scope.
    single<SchoolRepository> {
        SchoolRepositoryImpl(get())
    }
    // Provide view model instance.
    viewModel {
        SchoolViewModel(get())
    }
}
