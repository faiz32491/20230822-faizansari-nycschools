package com.app.nycschooldemo.repositories

import com.app.nycschooldemo.models.SATScoreInfoDTO
import com.app.nycschooldemo.models.SchoolInfoDTO
import com.app.nycschooldemo.network.NetworkResult
import kotlinx.coroutines.flow.Flow

/**
 * A repository interface that handles remote data operations for retrieving school information.
 */
interface SchoolRepository {
    /**
     * Gets the list of all the schools.
     *
     * @return Api response with the list of school.
     */
    suspend fun getSchoolsList(): Flow<NetworkResult<List<SchoolInfoDTO>>>

    /**
     * Gets the SAT data for the provided dbn.
     * @param dbn - A unique school id
     *
     * @return Api response with the list of SAT data.
     */
    suspend fun getSATData(dbn: String): Flow<NetworkResult<List<SATScoreInfoDTO>>>
}
