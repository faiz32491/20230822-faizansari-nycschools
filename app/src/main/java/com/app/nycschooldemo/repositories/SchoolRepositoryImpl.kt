package com.app.nycschooldemo.repositories

import com.app.nycschooldemo.models.SATScoreInfoDTO
import com.app.nycschooldemo.models.SchoolInfoDTO
import com.app.nycschooldemo.network.ApiService
import com.app.nycschooldemo.network.BaseApiResponse
import com.app.nycschooldemo.network.NetworkResult
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn

/**
 * An implementation of [SchoolRepository], to handle
 * remote data operations for getting schools and details.
 */
class SchoolRepositoryImpl(private val apiService: ApiService) : SchoolRepository, BaseApiResponse() {
    override suspend fun getSchoolsList(): Flow<NetworkResult<List<SchoolInfoDTO>>> {
        return flow {
            emit(safeCall { apiService.getSchoolsData() })
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getSATData(dbn: String): Flow<NetworkResult<List<SATScoreInfoDTO>>> {
        return flow {
            emit(safeCall { apiService.getSATData(dbn) })
        }.flowOn(Dispatchers.IO)
    }
}
