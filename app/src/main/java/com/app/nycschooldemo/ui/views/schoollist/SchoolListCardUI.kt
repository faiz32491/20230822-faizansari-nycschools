package com.app.nycschooldemo.ui.views.schoollist

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.nycschooldemo.R
import com.app.nycschooldemo.models.SchoolInfoDTO
import com.app.nycschooldemo.ui.views.common.Subtext
import com.app.nycschooldemo.utils.Utilities.getAddress

@Composable
fun SchoolListCardUI(
    schoolDataItem: SchoolInfoDTO,
    onCardClick: () -> Unit
) {
    Column(
        modifier = Modifier
            .background(color = colorResource(id = R.color.light_gray))
            .padding(10.dp)
    ) {
        Card(
            modifier = Modifier
                .fillMaxWidth()
                .clickable {
                    schoolDataItem.dbn?.let {
                        onCardClick()
                    }
                },
            colors = CardDefaults.cardColors(
                containerColor = Color.White
            ),
            elevation = CardDefaults.cardElevation(
                defaultElevation = 4.dp
            ),
            shape = RoundedCornerShape(size = 8.dp),
            border = BorderStroke(width = 1.dp, color = Color.Gray)
        ) {
            Column(
                modifier = Modifier.padding(all = 12.dp),
            ) {
                Text(
                    text = schoolDataItem.school_name.orEmpty(),
                    fontSize = 20.sp,
                    color = Color.Black,
                    fontWeight = FontWeight.Bold
                )
                Spacer(modifier = Modifier.height(2.dp))
                Subtext(
                    text = getAddress(
                        schoolDataItem.primary_address_line_1.orEmpty(),
                        schoolDataItem.city.orEmpty(),
                        schoolDataItem.state_code.orEmpty(),
                        schoolDataItem.zip.orEmpty()
                    )
                )
            }
        }
    }
}
