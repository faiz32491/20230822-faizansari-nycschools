package com.app.nycschooldemo.ui.views.schooldetails

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import com.app.nycschooldemo.models.SchoolInfoDTO
import com.app.nycschooldemo.ui.views.UiState
import com.app.nycschooldemo.ui.views.common.ErrorRetryUI
import com.app.nycschooldemo.ui.views.common.ShowLoader
import com.app.nycschooldemo.viewmodel.SchoolViewModel
import org.koin.androidx.compose.koinViewModel

@Composable
fun SchoolDetailsScreen(schoolInfo: SchoolInfoDTO) {
    val schoolViewModel: SchoolViewModel = koinViewModel()

    LaunchedEffect(key1 = true, block = {
        schoolInfo.dbn?.let { schoolViewModel.fetchSATData(it) }
    })

    val schoolDataUIState by schoolViewModel.uiState.observeAsState(UiState.Loading)

    when (schoolDataUIState) {
        // handle loading state when api called
        is UiState.Loading -> {
            ShowLoader()
        }
        // handle success state when api returns data
        is UiState.Success, UiState.Empty -> {
            SchoolDetailUi(
                schoolInfo,
                schoolViewModel.satScoreData.value,
            )
        }
        // handle error state when api called failed
        is UiState.Error -> {
            ErrorRetryUI {
                schoolInfo.dbn?.let { schoolViewModel.fetchSATData(it) }
            }
        }
    }
}
