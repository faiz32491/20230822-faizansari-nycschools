package com.app.nycschooldemo.ui.views.schoollist

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import com.app.nycschooldemo.models.SchoolInfoDTO
import com.app.nycschooldemo.ui.views.UiState
import com.app.nycschooldemo.ui.views.common.ErrorRetryUI
import com.app.nycschooldemo.ui.views.common.ShowLoader
import com.app.nycschooldemo.viewmodel.SchoolViewModel
import org.koin.androidx.compose.koinViewModel

@Composable
fun SchoolListScreen(
    schoolViewModel: SchoolViewModel = koinViewModel(),
    onCardClick: (SchoolInfoDTO) -> Unit,
) {
    val schoolDataUIState by schoolViewModel.uiState.observeAsState(UiState.Loading)

    when (schoolDataUIState) {
        // handle loading state when api called
        is UiState.Loading -> {
            ShowLoader()
        }
        // handle success state when api returns data
        is UiState.Success -> {
            schoolViewModel.schoolList.value?.let {
                SchoolTile(it) { schoolInfo ->
                    onCardClick(schoolInfo)
                }
            }
        }
        // handle error state when api called failed
        is UiState.Error, UiState.Empty -> {
            ErrorRetryUI {
                schoolViewModel.fetchSchoolList()
            }
        }
    }
}

@Composable
fun SchoolTile(
    schoolList: List<SchoolInfoDTO>,
    onCardClick: (SchoolInfoDTO) -> Unit,
) {
    LazyColumn {
        itemsIndexed(schoolList) { _, schoolDataItem ->
            SchoolListCardUI(
                schoolDataItem = schoolDataItem,
                onCardClick = {
                    onCardClick(schoolDataItem)
                },
            )
        }
    }
}
