package com.app.nycschooldemo.ui.views

sealed class UiState {
    object Loading : UiState()
    object Error : UiState()
    object Success : UiState()
    object Empty : UiState()
}