package com.app.nycschooldemo.ui.theme

import androidx.compose.ui.graphics.Color

val teal = Color(0xFF15799D)
val lightGray = Color(0xFFE4E5D9)
val darkBlue = Color(0xFF181D3C)
