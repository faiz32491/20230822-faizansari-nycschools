package com.app.nycschooldemo.ui.views.common

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import androidx.navigation.compose.currentBackStackEntryAsState
import com.app.nycschooldemo.R
import com.app.nycschooldemo.navigation.Route

@Composable
fun TopBar(
    navController: NavController,
) {
    val navBackStackEntry by navController.currentBackStackEntryAsState()
    val currentDestination = navBackStackEntry?.destination

    when (currentDestination?.route) {
        Route.Home.route -> {
            TitleBar(
                showBackButton = false,
            )
        }

        Route.SchoolDetails.route -> {
            TitleBar(
                showBackButton = true,
                onBackClick = { backNavigation(navController) },
            )
        }
    }
}

@Composable
fun TitleBar(
    showBackButton: Boolean = false,
    onBackClick: (() -> Unit)? = null,
) {
    TopAppBar(
        backgroundColor = MaterialTheme.colorScheme.primary,
    ) {
        Box(modifier = Modifier.fillMaxWidth()) {
            Text(
                text = stringResource(id = R.string.app_name),
                modifier = Modifier
                    .align(Alignment.Center),
                fontSize = 26.sp,
                color = Color.White,
                fontWeight = FontWeight.Bold,
            )

            if (showBackButton) {
                IconButton(
                    modifier = Modifier
                        .align(Alignment.CenterStart),
                    onClick = {
                        onBackClick?.invoke()
                    },
                ) {
                    Image(
                        painterResource(id = R.drawable.ic_back),
                        contentDescription = null,
                    )
                }
            }
        }
    }
}

private fun backNavigation(navController: NavController) {
    if (navController.previousBackStackEntry != null) {
        navController.navigateUp()
    }
}
