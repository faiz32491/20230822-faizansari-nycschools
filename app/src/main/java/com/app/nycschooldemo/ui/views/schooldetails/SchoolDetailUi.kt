package com.app.nycschooldemo.ui.views.schooldetails

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Scaffold
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Call
import androidx.compose.material.icons.rounded.LocationOn
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.app.nycschooldemo.R
import com.app.nycschooldemo.models.SATScoreInfoDTO
import com.app.nycschooldemo.models.SchoolInfoDTO
import com.app.nycschooldemo.ui.views.common.FloatingActionButton
import com.app.nycschooldemo.ui.views.common.Subtext
import com.app.nycschooldemo.utils.Utilities.dialPhoneNumber
import com.app.nycschooldemo.utils.Utilities.navigateToMaps

@Composable
fun SchoolDetailUi(schoolInfo: SchoolInfoDTO, satScoreInfoDTO: SATScoreInfoDTO?) {
    val context = LocalContext.current
    Scaffold(
        floatingActionButton = {
            ActionButtons(
                makeCall = {
                    dialPhoneNumber(schoolInfo.phone_number.orEmpty(), context)
                },
                openMap = {
                    if (schoolInfo.latitude.isNullOrEmpty()
                            .not() && schoolInfo.longitude.isNullOrEmpty().not()
                    ) {
                        navigateToMaps(
                            latitude = schoolInfo.latitude,
                            longitude = schoolInfo.longitude,
                            context = context,
                        )
                    }
                },
            )
        },
    ) { innerPadding ->
        Column(modifier = Modifier.padding(innerPadding)) {
            Column(
                modifier = Modifier
                    .padding(16.dp)
                    .verticalScroll(rememberScrollState()),
            ) {
                Text(
                    text = schoolInfo.school_name.orEmpty(),
                    fontSize = 20.sp,
                    color = Color.Black,
                    fontWeight = FontWeight.Bold,
                )
                Spacer(modifier = Modifier.height(20.dp))
                Text(
                    text = schoolInfo.overview_paragraph.orEmpty(),
                    fontSize = 12.sp,
                    color = Color.DarkGray,
                    fontWeight = FontWeight.Normal,
                )
                Spacer(modifier = Modifier.height(20.dp))

                Card(
                    modifier = Modifier
                        .fillMaxWidth(),
                    colors = CardDefaults.cardColors(
                        containerColor = colorResource(id = R.color.light_gray),
                    ),
                    elevation = CardDefaults.cardElevation(
                        defaultElevation = 4.dp,
                    ),
                    shape = RoundedCornerShape(size = 8.dp),
                    border = BorderStroke(width = 1.dp, color = Color.Gray),
                ) {
                    Column(
                        Modifier.padding(16.dp),
                        verticalArrangement = Arrangement.spacedBy(10.dp),
                    ) {
                        Subtext(
                            text = stringResource(
                                id = R.string.text_number_of_sat_takers,
                                satScoreInfoDTO?.num_of_sat_test_takers ?: "N/A",
                            ),
                        )
                        Subtext(
                            text = stringResource(
                                id = R.string.text_average_math_score,
                                satScoreInfoDTO?.sat_math_avg_score ?: "N/A",
                            ),
                        )
                        Subtext(
                            text = stringResource(
                                id = R.string.text_average_reading_score,
                                satScoreInfoDTO?.sat_critical_reading_avg_score ?: "N/A",
                            ),
                        )
                        Subtext(
                            text = stringResource(
                                id = R.string.text_average_writing_score,
                                satScoreInfoDTO?.sat_writing_avg_score ?: "N/A",
                            ),
                        )
                    }
                }
            }
        }
    }
}

@Composable
fun ActionButtons(
    makeCall: () -> Unit,
    openMap: () -> Unit,
) {
    Column(
        verticalArrangement = Arrangement.spacedBy(10.dp),
    ) {
        FloatingActionButton(
            onClick = { makeCall() },
            icon = Icons.Rounded.Call,
            contentDescription = "Call Icon"
        )
        FloatingActionButton(
            onClick = { openMap() },
            icon = Icons.Rounded.LocationOn,
            contentDescription = "Location Icon"
        )
    }
}
