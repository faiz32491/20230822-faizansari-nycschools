package com.app.nycschooldemo.ui.views.common

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp

@Composable
fun Subtext(
    text: String,
) {
    Text(
        text = text,
        fontSize = 16.sp,
        color = Color.Black,
        fontWeight = FontWeight.Normal,
    )
}
