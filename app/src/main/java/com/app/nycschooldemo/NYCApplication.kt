package com.app.nycschooldemo

import android.app.Application
import com.app.nycschooldemo.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.component.KoinComponent
import org.koin.core.context.GlobalContext

class NYCApplication : Application(), KoinComponent {

    override fun onCreate() {
        super.onCreate()
        // Koin initialisation
        GlobalContext.startKoin {
            androidLogger()
            androidContext(this@NYCApplication)
            modules(appModule)
        }
    }
}