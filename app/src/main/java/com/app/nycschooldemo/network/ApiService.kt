package com.app.nycschooldemo.network

import com.app.nycschooldemo.models.SATScoreInfoDTO
import com.app.nycschooldemo.models.SchoolInfoDTO
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * ApiService interface to communicate with server.
 */
interface ApiService {
    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchoolsData(): Response<List<SchoolInfoDTO>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSATData(@Query("dbn") dbn: String): Response<List<SATScoreInfoDTO>>
}
