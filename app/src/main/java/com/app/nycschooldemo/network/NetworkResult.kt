package com.app.nycschooldemo.network

/**
 * A sealed class for network result
 */
sealed class NetworkResult<T>(
    val data: T? = null,
    val message: String? = null,
    val responseCode: Int? = null,
) {
    class Success<T>(data: T?) : NetworkResult<T>(data)
    class Failure<T>(message: String, responseCode: Int? = null, data: T? = null) :
        NetworkResult<T>(data, message, responseCode)
}
